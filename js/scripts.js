jQuery.noConflict();


/***
* Sledování odkazů přes GA - lze použít jak vnitřně, tak venkovně
* 
* @param category
* @param event
* @param label
* @param url
* 
* @returns {Boolean}
*/
function gatrackeventnormalwaitforurl(category, event, label, url) {
	// mame ga?
	if(window._gat){
		// nastav callback
		_gaq.push(['_set','hitCallback',
							function(){document.location=url;}
		]);
		// registruj udalost
		_gaq.push(['_trackEvent', category, event, label]); 
		// zrus pokracovani odkazu - bude provedeno asynchronnim callbackem
		return false;
	} 
	// nemame - normalne klikni
	else {
		return true;
	}
}

jQuery(document).ready(function (){
	
	// xart:ml schovame nabidku valtra open days u poptavkoveho formulare
	var parser = document.createElement('a');
	parser.href = document.URL;
	if (parser.pathname.indexOf("/kontakt") > -1 && parser.pathname.split("/").length > 2) {
		jQuery(".mod_custom.top").hide();
	}
	// prohodime poradi
	jQuery(".top").insertAfter(".mod_breadcrumbs").insertAfter(".componentheading");
	
	/*
	xart:ml měření akcí: schovávám, už změřeno
	jQuery(".akce.valtra.open a").click(function() {
	  	var link = this.href;
	   	gatrackeventnormalwaitforurl('content-valtra', 'click', 'open days left column', link);
	});
	jQuery(".akce.valtra.le a").click(function() {
	  	var link = this.href;
	   	gatrackeventnormalwaitforurl('content-valtra', 'click', 'limited edition left column', link);
	});
	jQuery(".mod_custom.top a").click(function() {
	  	var link = this.href;
	   	gatrackeventnormalwaitforurl('content-den-pouze-jeden-tahak', 'click', 'top', link);
	});
	*/
	

/* easing */
jQuery.easing.easeInOutBack = function (x, t, b, c, d, s) {
  if (s == undefined) s = 1.70158;
  if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
  return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
};

jQuery('.mod_mainmenu').each(function(){
	jQuery(this).prepend('<button class="toggle">Menu</button>');
	jQuery('.toggle',this).on('click',function(){
		jQuery('body').toggleClass('mod_mainmenu-on');
	});
});

/* slideshow */
jQuery('.mod_custom-slideshow .in').after('<p class="nav"/>');
jQuery('.mod_custom-slideshow ul.slider').cycle({easing:'easeInOutBack',fx: 'custom',cssBefore:{left:2000,display:'block'},animIn:{left:0},animOut:{left:2000},pager:'.mod_custom-slideshow .nav',speedIn:1000,speedOut:1200,delay:3000,timeout:5000,cleartypeNoBg: true,cleartype: true,pause:1});


jQuery('.mod_banners').jcarousel({scroll:1,easing:'easeInOutBack'});


/* produkt detail tabs */
jQuery('ul.product-tabs').tabs('.product-panes > .pane',{tabs:'li',current:'act'});



/* button */
jQuery('.right-button').wrap('<span class="right-button-wrap"/>');
jQuery('.right-button-wrap').append('<span class="right-button-before"/>');
jQuery('.right-button').hover(function (){jQuery(this).next('.right-button-before').toggleClass('hover')});
jQuery('.right-button').mousedown(function (){jQuery(this).next('.right-button-before').addClass('active')}).mouseup(function (){jQuery(this).next('.right-button-before').removeClass('active')});


  jQuery('h2.toggle').click(function(){
    jQuery('div.toggle-content').toggle(200);
  });

/* contactTab */

jQuery(".mtContent .header a").click(function() {
    var el = jQuery(this).attr('href');
    jQuery(this).parents('.mtContent').find('a').removeClass('act');
    jQuery('a[href=' + el + ']').addClass('act');
    jQuery(this).parents(".mtContent").find('.opcl').removeClass ('act');
    jQuery(this).parents(".mtContent ").find(el).addClass('act');
    return false;
});

});